# HTML & CSS

HTML is the standard markup language for creating web pages and web applications.
CSS is a "style sheet language" used for describing the presentation of a webpage.

There's plenty of resources on the Web for learning both HTML and CSS. 

[https://www.codecademy.com/learn/web](https://www.codecademy.com/learn/web) is a great place to start.

## Question 1.1
Write a CSS selector that selects every `tr` element that has the class `flight`.

## Question 1.2
Write a CSS selector that selects every `p` element that is contained in any `div` that has the class `cat` or `dog`.

## Question 1.3
Take a look at a mock flight website that we created.

[https://intense-crag-25795.herokuapp.com/](https://intense-crag-25795.herokuapp.com/)

Write a CSS selector that selects the "Departure Date" input field.

# JavaScript

Javascript is a scripting language used in web pages. Again, [https://www.codecademy.com/learn/javascript](https://www.codecademy.com/learn/javascript) is a great resource to use to learn Javascript.

jQuery is a library written in JavaScript that is used on websites. It helps to interact with the HTML elements on the page. Again, Codecademy is a good resource for this.

See if you can answer the following questions.

## Question 2.1
Write a program in JavaScript that will log to console every number between 1 and 100.

## Question 2.2
Write a command in jQuery that will apply a red border to every `input` element.

## Question 2.3
Again, take a look at our mock flight website.

[https://intense-crag-25795.herokuapp.com/](https://intense-crag-25795.herokuapp.com/)

Write a script that will fill in the "Number of Adults" field with the number `4`, the "Number of Children" field with `2`, and the "Number of Infants" field with `2`.

# Regular Expressions

Regular expressions are sequences of characters that define a string pattern. They are used to find certain pieces of text inside of a larger amount of text.

We'll need you to learn how to use them! There are many free resources available to help you learn them. [https://regexone.com/](https://regexone.com/) is one helpful resource.

Once you learn the basics and you want to try out your own regular expressions, this site can help you test them out:

[http://regexr.com/](http://regexr.com/)

## Question 3.1
Write a regular expression that matches only the words `cat`, `cot`, and `cut`.

## Question 3.2
Write a regular expression that matches a number that has a decimal place. For example, it should match `1.0`, `54.65`, `123.4567` and `99.99`. Don't worry about numbers that have commas or numbers that don't have decimal points.

## Question 3.3
Write a regular expression that matches any three letter flight code, written in capital letters, like `MEL` or `SYD`.